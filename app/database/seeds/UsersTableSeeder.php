<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 30) as $index)
		{
			$usr = Sentry::register(array(
				'username' =>  $faker->firstName,
			    'email'    => $faker->email,
			    'password' => Hash::make($faker->phoneNumber),
			    'activated' => true
			));

			$grp = Sentry::findGroupById(2);

			$usr->addGroup($grp);
		}
	}

}