<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserFollowTable extends Migration {

	public function up()
	{
		Schema::create('user_follow', function(Blueprint $table) {
			$table->increments('id')->unsigned;
			$table->timestamps();
			$table->integer('user_id')->unsigned();
			$table->integer('follower_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('user_follow');
	}
}