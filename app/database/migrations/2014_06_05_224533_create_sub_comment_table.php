<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubCommentTable extends Migration {

	public function up()
	{
		Schema::create('sub_comment', function(Blueprint $table) {
			$table->increments('id')->unsigned;
			$table->timestamps();
			$table->integer('comment_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->string('content', 200);
		});
	}

	public function down()
	{
		Schema::drop('sub_comment');
	}
}