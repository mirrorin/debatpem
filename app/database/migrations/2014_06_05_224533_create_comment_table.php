<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Kalnoy\Nestedset\NestedSet;

class CreateCommentTable extends Migration {

	public function up()
	{
		Schema::create('comment', function(Blueprint $table) {
			$table->increments('id')->unsigned;
			$table->timestamps();
			$table->integer('post_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->string('content', 200);
			$table->string('slug');

			NestedSet::columns($table);
		});
	}

	public function down()
	{
		Schema::drop('comment');
	}
}