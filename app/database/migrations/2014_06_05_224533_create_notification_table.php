<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationTable extends Migration {

	public function up()
	{
		Schema::create('notification', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('user_id')->unsigned();
			$table->integer('post_id')->unsigned();
			$table->integer('comment_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('notification');
	}
}