<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('socials', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('no action')
						->onUpdate('no action');
		});
		Schema::table('user_details', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('no action')
						->onUpdate('no action');
		});
		Schema::table('throttle', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('no action')
						->onUpdate('no action');
		});
		Schema::table('post', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('comment', function(Blueprint $table) {
			$table->foreign('post_id')->references('id')->on('post')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('comment', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('sub_comment', function(Blueprint $table) {
			$table->foreign('comment_id')->references('id')->on('comment')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('sub_comment', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('notification', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('notification', function(Blueprint $table) {
			$table->foreign('post_id')->references('id')->on('post')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('notification', function(Blueprint $table) {
			$table->foreign('comment_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('user_follow', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('user_follow', function(Blueprint $table) {
			$table->foreign('follower_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::table('socials', function(Blueprint $table) {
			$table->dropForeign('socials_user_id_foreign');
		});
		Schema::table('user_details', function(Blueprint $table) {
			$table->dropForeign('user_details_user_id_foreign');
		});
		Schema::table('throttle', function(Blueprint $table) {
			$table->dropForeign('throttle_user_id_foreign');
		});
		Schema::table('post', function(Blueprint $table) {
			$table->dropForeign('post_user_id_foreign');
		});
		Schema::table('comment', function(Blueprint $table) {
			$table->dropForeign('comment_post_id_foreign');
		});
		Schema::table('comment', function(Blueprint $table) {
			$table->dropForeign('comment_user_id_foreign');
		});
		Schema::table('sub_comment', function(Blueprint $table) {
			$table->dropForeign('sub_comment_comment_id_foreign');
		});
		Schema::table('sub_comment', function(Blueprint $table) {
			$table->dropForeign('sub_comment_user_id_foreign');
		});
		Schema::table('notification', function(Blueprint $table) {
			$table->dropForeign('notification_user_id_foreign');
		});
		Schema::table('notification', function(Blueprint $table) {
			$table->dropForeign('notification_post_id_foreign');
		});
		Schema::table('notification', function(Blueprint $table) {
			$table->dropForeign('notification_comment_id_foreign');
		});
		Schema::table('user_follow', function(Blueprint $table) {
			$table->dropForeign('user_follow_user_id_foreign');
		});
		Schema::table('user_follow', function(Blueprint $table) {
			$table->dropForeign('user_follow_follower_id_foreign');
		});
	}
}