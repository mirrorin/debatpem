<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getIndex(){


		$data['post'] = Post::whereHas('user', function($q){
			$q->orderBy('views');
		})->paginate(3);
		return View::make('depan.index', $data);
	}

	public function postLogin(){
        try
        {
            $credentials = array(
                'email'    => Input::get('email'),
                'password' => Input::get('pass'),
            );

            $user = Sentry::authenticate($credentials, Input::get('remember'));
        }
        catch(\Cartalyst\Sentry\Throttling\UserBannedException $e)
        {
            return Response::json(array('logged' => false, 'errorMessage' => 'User Banned', 'errorType' => 'danger'));
        }
        catch (RuntimeException $e)
        {
            return Response::json(array('logged' => false, 'errorMessage' => 'Failed Login SU!', 'errorType' => 'danger'));
        }

        return Response::json(array('logged' => true));
	}

	public function postRegister()
	{
		try
		{
		    // Create the user
		    $user = Sentry::createUser(array(
		        'email'     => Input::get('email'),
		        'username' => Input::get('username'),
		        'password'  => Input::get('password'),
		        'activated' => true
		    ));

		    // Find the group using the group id
		    $userGroup = Sentry::findGroupById(2);

		    // Assign the group to the user
		    $user->addGroup($userGroup);
		    
				$userlogin = Sentry::findUserByLogin(Input::get('email'));
				Sentry::login($userlogin, false);		
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
		    return Response::json(['register' => false, 'errorMessage' => 'User Exist!']);
		}

		return Response::json(['register' => true, 'successMessage' => 'Silahkan Login Untuk Melanjutkan', 'url' => Url::to('/')]);
	}

	public function postLoginPlease(){
			$email = Input::get('email');

			$user = Sentry::findUserByLogin($email);
			Sentry::login($user, false);

			return Redirect::to('/');
		
	}

	public function getDashboard(){
		return View::make('belakang.index');
	}

	public function getLogout(){
		Sentry::logout();
		return Redirect::route('getHome');
	}

	public function getSemua(){
		return View::make('api.semua');
	}

	public function getAbout(){
		return View::make('etc.about');
	}

	public function getProfile(){
		return View::make('belakang.profile');
	}

}
