<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::group(array('before' => 'pakeLogin'), function(){
    Route::get('', [
        'as' => 'dash',
        'uses' => 'dashboardController@getIndex'
        ]);

    Route::get('bikin-perkara', [
        'as' => 'bikinPost',
        'uses' => 'PostController@getCreate'
        ]);

    Route::post('bikin-post', [
        'as' => 'postCreatePost',
        'uses' =>'PostController@postCreatePost'
        ]);

    Route::get('testing', function(){

        $data = DB::table('post_user')
                ->where('post_id', '=', 1)
                ->where('tanggapan', '=', 'kontra')
                ->count();

        return $data;
    });

    Route::get('mintajson.json', function(){
        // $post = Post::all();
        $json = Conner\Tagging\Tag::where('count', '>=', 1)->get();
        foreach ($json as $key => $value) {
            $data[] = strtolower($value['name']);
        }
        return Response::json($data);
    });

    Route::get('user/logout', [
        'as' => 'getLogout',
        'uses' => 'HomeController@getLogout'
    ]);

    Route::post('mintakejelasan', [
        'as' => 'postTanggapan',
        'uses' =>'PostController@postTanggapan'
        ]);

    Route::post('komeng', [
        'as' => 'postComment',
        'uses' => 'PostController@postComment'
        ]);

    Route::get('api', [
        'as' => 'getSemua',
        'uses' =>'HomeController@getSemua'
    ]);

    Route::post('p/postCreateSub',[
        'as' => 'postCreateSub',
        'uses' => 'PostController@postCreateSub'
        ]);

});

Route::group(['before' => 'guest'], function(){
    Route::get('', [
        'as' => 'getHome',
        'uses' => 'HomeController@getIndex']
    );

    Route::post('login', [
            'as' => 'postLogin',
            'uses' => 'HomeController@postLogin'
            ]);

    Route::post('daftar', [
            'as' => 'postRegister',
            'uses' => 'HomeController@postRegister'
            ]);

    Route::get('about', [
        'as' => 'getAbout',
        'uses' =>'HomeController@getAbout'
    ]);

    Route::post('postLoginPlease', [
        'as' => 'postLoginPlease',
        'uses' => 'HomeController@postLoginPlease'
    ]);
      
    Route::get('{slug}', function($slug){
            Session::put('attemptedUrl', URL::current());
            // $data['comment'] = $table->comment;
            $data = Post::where('slug', '=', $slug)->first();
            // $data->id
            // $data['comment'] = Post::find($data->id)->comment()->orderBy('created_at', 'desc')->get();
            $data['comment'] =  DB::table('post')
                ->where('post.slug', '=', $slug)
                ->orderBy('comment.created_at', 'desc')
                // ->leftjoin('post_user', 'post.id', '=', 'post_user.post_id')
                ->rightjoin('comment', 'post.id', '=','comment.post_id')
                ->select('comment.id', 'comment.content', 'comment.user_id', 'comment.slug', 'comment.created_at')
                ->get();

            $updateviews = Post::find($data->id);
            $updateviews->views += 1;
            $updateviews->save();
                
            $data['int'] =DB::table('post')
                ->where('post.slug', '=', $slug)
                ->orderBy('comment.created_at', 'desc')
                // ->leftjoin('post_user', 'post.id', '=', 'post_user.post_id')
                ->rightjoin('comment', 'post.id', '=','comment.post_id')
                ->select('comment.id', 'comment.content', 'comment.user_id', 'comment.slug', 'comment.created_at')
                ->count();
            $data['data'] = Post::where('slug', '=', $slug)->first();
            return View::make('depan.post', $data);
            // return Response::json($comments);
    });

    Route::get('tags/{tags}', function($slug){
        $data['post'] = Post::withAnyTag($slug)->paginate(3);
        return View::make('belakang.index', $data);
    });

    Route::get('u/{name}', function($name){
        $user = User::where('username', '=', $name)->first();
        $data['isi'] = $user;

        return View::make('belakang.profile',$data);
    });
  
});