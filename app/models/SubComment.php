<?php

class SubComment extends Eloquent {

	protected $table = 'sub_comment';
	public $timestamps = true;

	public function comment()
	{
		return $this->belongsTo('Comment');
	}

}