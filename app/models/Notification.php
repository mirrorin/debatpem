<?php

class Notification extends Eloquent {

	protected $table = 'notification';
	public $timestamps = true;

	public function notification()
	{
		return $this->belongsToMany('Notification');
	}

}