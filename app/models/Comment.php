<?php

class Comment extends Eloquent {

	protected $table = 'comment';
	public $timestamps = true;

	public function post()
	{
		return $this->belongsToMany('Post');
	}

	public function subComment()
	{
		return $this->hasMany('SubComment');
	}

}