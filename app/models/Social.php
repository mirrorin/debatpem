<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Social extends Eloquent {

	protected $table = 'socials';
	public $timestamps = true;

	use SoftDeletingTrait;

	protected $dates = ['deleted_at'];

	public function user()
	{
		return $this->belongsTo('User');
	}

}