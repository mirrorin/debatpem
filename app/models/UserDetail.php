<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class UserDetail extends Eloquent {

	protected $table = 'user_details';
	public $timestamps = true;

	use SoftDeletingTrait;

	protected $dates = ['deleted_at'];

}