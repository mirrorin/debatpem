<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class User extends Eloquent {

	protected $table = 'users';
	public $timestamps = true;
	use SoftDeletingTrait;

	protected $dates = ['deleted_at'];

	public function socials()
	{
		return $this->hasMany('Social');
	}

	public function details()
	{
		return $this->hasMany('UserDetail');
	}

	public function throttles()
	{
		return $this->hasMany('Throttle');
	}

	public function groups()
	{
		return $this->belongsToMany('Group', 'users_groups');
	}

	public function notification()
	{
		return $this->hasMany('Notification');
	}

	public function post()
	{
		return $this->hasOne('Post');
	}

	public function follow()
	{
		return $this->belongsToMany('User', 'user_follow', 'user_id', 'follower_id');
	}

	public function followers()
	{
		return $this->belongsToMany('User', 'user_follow', 'follower_id', 'user_id');
	}

}