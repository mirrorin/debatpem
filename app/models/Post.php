<?php

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
class Post extends Eloquent implements SluggableInterface{
	use Conner\Tagging\Taggable;

	use SluggableTrait;

    protected $sluggable = array(
        'build_from' => 'title',
        'save_to'    => 'slug',
    );

	protected $table = 'post';
	public $timestamps = true;

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function comment()
	{
		return $this->hasMany('Comment');
	}

}