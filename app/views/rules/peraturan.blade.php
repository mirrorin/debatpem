<div class="col-md-4">
	<legend>Countdown Election Day</legend>
	<div class="post-thumb">
		<div class="post-legend list-group-item">
			<p>
				<ul>
					<h4>
						<span id="clock"></span>
					</h4>	
				</ul>
			</p>
		</div>
	</div>
</div>
<br>
<div class="col-md-4">
	<legend>Rules</legend>
	<div class="post-thumb">
		<div class="post-legend list-group-item">
			<p>
				<ul>
					<h4>

					<p><span class="label label-success">1.</span>
						  </p>
					<p><span class="label label-success">2.</span>
						  Dilarang Sara</p>
					<p><span class="label label-success">3.</span>
						  Dilarang </p>
					<p><span class="label label-success">4.</span>
						  Dilarang</p>
					<p><span class="label label-success">5.</span>
						  Dilarang</p>

				</h4>	
				</ul>
			</p>
		</div>
	</div>
</div>
@section('js')
<script type="text/javascript">
  // 15 days from now!
  var date = new Date(new Date().valueOf() + 20 * 24 * 60 * 60 * 1000);
  $('#clock').countdown(date, function(event) {
    $(this).html(event.strftime('%D Days %H:%M:%S'));
  });
</script>
@stop