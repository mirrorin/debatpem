@extends('layouts.master')

@section('css')
    <link rel="stylesheet" href="assets/css/bootstrap3-wysiwyg5.css">
    <link rel="stylesheet" href="assets/css/bootstrap-tagsinput.css">
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <form id="post-form" method="POST"   role="form">            
                <div class="form-group">
                    <input type="text" class="form-control" id="title" placeholder="Buat Judul Perkara!">
                </div>
                <div class="form-group">
                    <textarea id="some-textarea" class="form-control" placeholder="Apa Permasalahan kamu?" rows="20"></textarea> 
                </div>
                <div class="form-group">
                    <input type="text" id="tag" value="" data-role="tagsinput" class="ngetag form-control"/>
                </div>
                <input type="submit" class="btn btn-primary" value="Submit">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="reset" class="btn btn-danger pull-right">Cancel</button>
            </form>
        </div>
	@include('rules.peraturan')
    </div>
</div>
@stop

@section('js')
<script type="text/javascript" src="assets/js/typeahead.min.js"></script>
<script type="text/javascript" src="assets/js/wysihtml5-0.3.0.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap3-wysihtml5.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-tagsinput.js"></script>
<script type="text/javascript">
    $('#some-textarea').wysihtml5();
</script>
<script type="text/javascript">
     $(function(){
            $('#post-form').on('submit', function() 
            {                
                $.ajax({
                    "type": "POST",
                    "url": "{{URL::route('postCreatePost')}}",
                    "data": {
                        "title" : $('#title').val(),
                        "content" : $('#some-textarea').val(), 
                        "tags" : $("#tag").tagsinput('items')
                    },
                    "dataType": "json"
                }).done(function(result) 
                { 
                    $('.bottom-right').notify({
                        type: 'success',
                        fadeOut : {
                            enabled : true,
                            delay : 3000
                        },
                        message: {
                            text:'Post Published!',
                        }
                      }).show();

                    window.location = result.slug;
                });                
                return false;
            });
        });
</script>
@stop
