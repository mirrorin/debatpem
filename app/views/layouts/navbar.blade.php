<nav class="navbar navbar-default navbar-fixed-top" role="navigation">

	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="{{URL::to('')}}">Debat Pemilu</a>
	</div>

	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse navbar-ex1-collapse">
		<ul class="nav navbar-nav">
		</ul>
        @if(Sentry::check())
            <form class="navbar-form navbar-left col-md-12">
              <input type="text" class="form-control" placeholder="Search">
            </form>
        @endif
		<ul class="nav navbar-nav navbar-right">
        @if(!Sentry::check())
            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>&nbsp; Please Sign In. To Join Debate Area
               <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <form id="login-form" method="post">
                        <div class="navbar-content">
                            <div class="row">
                                <div class="col-md-12">  
                                    	<div class="form-group">
                                    		<input type="text" class="form-control" id="email" placeholder="Email">
                                    	</div>
                                    	<div class="form-group">
                                    		<input type="password" class="form-control" id="pass" placeholder="Password">
                                    	</div>
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label class="" style="color:#FFF">
                                                    <input type="checkbox" class="" id="remember">Remember me
                                                </label>
                                            </div>
                                        </div>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="navbar-footer">
                            <div class="navbar-footer-content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#" class="btn btn-default btn-sm">Not Registered?</a>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="submit" value="Login" class="btn btn-default btn-sm pull-right">
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </li>
                </ul>
            </li>
        @else
            <li><a href="{{URL::route('bikinPost')}}" class="btn btn-primary btn-sm active">Bikin Perkara</a></li>
            <li><a href="{{URL::route('getSemua')}}" class="btn btn-primary btn-sm active">Repositori Data</a></li>
            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>&nbsp; {{Sentry::getUser()->username}}
               <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <div class="navbar-content">
                            <div class="row">
                                <div class="col-md-5">
                                    <img src="{{Gravatar::src(Sentry::getUser()->email)}}" alt="Alternate Text" class="img-responsive"></a> <br>
                                </div>
                                <div class="col-md-7">
                                    <a href="#">{{Sentry::getUser()->first_name}} {{Sentry::getUser()->last_name}}</a>
                                    <p class="text-muted small">
                                        {{Sentry::getUser()->email}}
                                    </p>
                                    <div class="divider">
                                    </div>
                                    <a href="#" class="btn btn-primary btn-sm active">View Profile</a>
                                </div>
                            </div>
                        </div>
                        <div class="navbar-footer">
                            <div class="navbar-footer-content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#" class="btn btn-default btn-sm">Change Passowrd</a>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="{{URL::route('getLogout')}}" class="btn btn-default btn-sm pull-right">Sign Out</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
        @endif
        </ul>
	</div><!-- /.navbar-collapse -->
</nav>

@section('js')

@stop